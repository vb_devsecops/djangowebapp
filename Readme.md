Step1: Activate the virutal environment 

        $ cd ~

        $ source django_project/bin/activate

Step:2 Create the app

	$ cd ~/projects/simplewebapp/djangowebapp

	$ python3 manage.py startapp <appname>(ex: travello)

Step3: Run the sever

        $ cd ~/projects/simplewebapp/djangowebapp/

        $ python3 manage.py runserver

Step4: Push changes to Gitlab
        $ git add *

        $ git commit -m "message"
 
        $ git push -u origin main
        
